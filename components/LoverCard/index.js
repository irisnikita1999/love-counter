// Libraries
import React from "react";
import Image from "next/image";
import PropTypes from "prop-types";

const LoverCard = ({ imageUrl, color, loverName }) => {
  return (
    <div className="flex flex-col items-center justify-center">
      <div
        className="relative overflow-hidden w-28 h-28 rounded-full border-4 "
        style={{ borderColor: color }}
      >
        <Image
          src={imageUrl}
          objectFit="cover"
          objectPosition="center"
          layout="fill"
          loading="eager"
          alt="Boy friend - Nguyen Luong Truong Vi"
        />
      </div>
      <h2
        style={{
          textAlign: "center",
          fontSize: 20,
          marginTop: 10,
          color: color,
        }}
      >
        {loverName}
      </h2>
    </div>
  );
};

LoverCard.propTypes = {
  imageUrl: PropTypes.string,
  borderColor: PropTypes.string,
  loverName: PropTypes.string,
};

LoverCard.defaultProps = {
  imageUrl: "/images/boy.jpg",
  borderColor: "#eb2f96",
  loverName: "No Name",
};
export default LoverCard;
