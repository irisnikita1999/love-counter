// Libraries
import React, { useState, useEffect, useMemo } from "react";
import TypeIt from "typeit";
import Cookies from "js-cookie";
import { Howl } from "howler";

// Components
import Layout from "components/Layout";

import Introduce from "modules/Introduce";

const sounds = ["/sound/mouse-hover-duck.mp3", "/sound/pho-da-len-den.mp3"];

export default function Home() {
  const [isLoading, setLoading] = useState(true);
  const [isVisibleBtn, setVisibleBtn] = useState(false);
  const [isHoverDisableBtn, setHoverDisableBtn] = useState(false);
  const [isPlaying, setPlaying] = useState(false);
  const howlSounds = useMemo(() => {
    return [...sounds].map(
      (sound) =>
        new Howl({
          src: [sound],
          autoplay: false,
          onplay() {
            if (sound === "/sound/pho-da-len-den.mp3") {
              setPlaying(true);
            }
          },
          onend() {
            if (sound === "/sound/pho-da-len-den.mp3") {
              setPlaying(false);
            }
          },
        })
    );
  }, []);

  const [sections] = useState([
    {
      name: "section-1",
      label: "Đếm ngày yêu",
      component(isLoading, isPlaying, howlSounds, setPlaying) {
        return (
          <Introduce
            isLoading={isLoading}
            isPlaying={isPlaying}
            howlSounds={howlSounds}
            setPlaying={setPlaying}
          />
        );
      },
    },
    {
      name: "section-2",
      label: "Lịch sử yêu đương",
      component() {
        return (
          <div className="animate__animated animate__fadeIn">
            Hello lich su yeu duong
          </div>
        );
      },
    },
  ]);
  const [currentSection, setCurrentSection] = useState(0);

  useEffect(() => {
    if (Cookies.get("visited")) {
      setLoading(false);

      return;
    }

    setTimeout(() => {
      new TypeIt("#loading-text", {
        speed: 60,
        waitUntilVisible: true,
      })
        .type("Hi! Xin chào người đẹp của lòng mình", { delay: 300 })
        .delete()
        .type("Mình đã để ý bạn từ lâu lắm rồi!", { delay: 300 })
        .delete()
        .type("Bạn cho mình làm nha!", { delay: 300 })
        .move(-4, { delay: 200 })
        .type("quen ")
        .move("END", { delay: 300 })
        .type(" ^^", { delay: 300 })
        .exec(() => {
          setVisibleBtn(true);
        })
        .go();
    }, 100);
  }, []);

  const onClickAccept = () => {
    howlSounds[0].play();

    setTimeout(() => {
      document.getElementById("loading-wrap").classList.add("animate__fadeOut");

      Cookies.set("visited", true, { expires: 1, path: "" });

      setTimeout(() => {
        setLoading(false);

        howlSounds[1].play();
      }, 1000);
    }, 300);
  };

  const renderSection = () => {
    switch (currentSection) {
      case 0:
        return sections[0].component(
          isLoading,
          isPlaying,
          howlSounds,
          setPlaying
        );
      case 1:
        return sections[1].component();

      default:
        return <div>No content</div>;
    }
  };

  return (
    <Layout>
      <div className="layout-wrap">
        <div
          className={`${
            isLoading
              ? "opacity-0"
              : "opacity-100 animate__animated animate__fadeIn"
          } glass-card flex flex-col justify-center items-center container mx-auto text-gray-700 lg:h-auto h-full lg:rounded-3xl rounded-none overflow-hidden`}
          style={{
            minHeight: "90vh",
            fontFamily: "lobster",
          }}
        >
          <div className="glass-bg"></div>
          <div className="outer-scratch">
            <div className="inner-scratch">
              <div className="background grain"></div>
            </div>
          </div>
          <div className="absolute right-10">
            {sections.map((section, index) => (
              <div
                key={section.name}
                className={` transform ${
                  currentSection === index
                    ? "scale-125 opacity-100"
                    : "scale-100 opacity-80"
                } w-3 h-3 duration-500  bg-gray-800 mb-2 rounded-full cursor-pointer opacity-80 hover:opacity-100 transition-all`}
                onClick={() => {
                  setCurrentSection(index);
                }}
              ></div>
            ))}
          </div>
          {renderSection()}
        </div>
        <div
          id="loading-wrap"
          className={`${
            isLoading ? "fixed" : "hidden"
          } animate__animated top-0 z-20 h-screen w-screen bg-white flex flex-col items-center justify-center`}
        >
          <h2
            id="loading-text"
            className="text-2xl font-semibold space-now text-center w-full"
          ></h2>
          <div
            className={`animate__animated ${
              isVisibleBtn ? "flex animate__fadeIn" : "hidden"
            } items-center mt-5`}
          >
            <button
              id="btn-accept"
              className="py-2 px-5 w-44 text-white rounded-md mr-5 hover:opacity-80 transition-all"
              style={{
                backgroundColor: isHoverDisableBtn ? "#cf1322" : "#08979c",
              }}
              onClick={onClickAccept}
            >
              {isHoverDisableBtn ? "No, Ứ chịu đâu!" : "Yes, Mình đồng ý!"}
            </button>
            &nbsp;
            <button
              id="btn-disable"
              className="py-2 px-5 text-white w-44 rounded-md  hover:opacity-80 transition-all"
              style={{
                backgroundColor: isHoverDisableBtn ? "#08979c" : "#cf1322",
              }}
              onClick={onClickAccept}
              onMouseEnter={() => {
                setHoverDisableBtn(true);
              }}
              onMouseLeave={() => {
                setHoverDisableBtn(false);
              }}
            >
              {isHoverDisableBtn ? "Yes, Mình đồng ý!" : "No, Ứ chịu đâu!"}
            </button>
          </div>
        </div>
      </div>
    </Layout>
  );
}
