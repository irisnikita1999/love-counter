// Libraries
import React from "react";
import PropTypes from "prop-types";
import Head from "next/head";

const Layout = ({ meta, children }) => {
  const { title, description, keywords, ogs } = meta;

  return (
    <div id="layout">
      <Head>
        <title>{title}</title>
        <meta content="text/html;charset=UTF-8" />
        <meta name="keywords" content={keywords.join(", ")} />
        <meta name="description" content={description} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta property="og:title" content={ogs.title || title} />
        <meta
          property="og:description"
          content={ogs.description || description}
        />
        <meta property="og:image" content={ogs.image} />
        <meta property="og:url" content={ogs.url} />
      </Head>
      {children}
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node,
  meta: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    author: PropTypes.string,
    ogs: PropTypes.shape({
      image: PropTypes.string,
      url: PropTypes.string,
      title: PropTypes.title,
      description: PropTypes.string,
    }),
  }),
};

Layout.defaultProps = {
  meta: {
    title: "Dating - Vi ❤️ Yen",
    description: "Thanks for alway been with me, my girl!",
    keywords: ["Love", "Dating", "Counter", "My wife", "Vi", "Yen"],
    ogs: {
      image: "/images/meta.jpg",
      url: "",
      title: "Dating - Vi ❤️ Yen",
      description: "Thanks for alway been with me, my girl!",
    },
  },
};

export default Layout;
