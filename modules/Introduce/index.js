import React, { useState } from "react";
import PropTypes from "prop-types";
import Image from "next/image";
import Slider from "react-slick";

// Components
import LoverCard from "components/LoverCard";

// Icons
import LoveIcon from "icons/LoveIcon";
import moment from "moment";

const settingSlider = {
  dots: false,
  autoplay: true,
  arrows: false,
  infinite: true,
  speed: 2000,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
      },
    },
  ],
};

const Introduce = ({ isPlaying, howlSounds, setPlaying }) => {
  const [stories] = useState([
    { id: 1, src: "/images/story/story-01.jpg" },
    { id: 2, src: "/images/story/story-02.jpg" },
    { id: 3, src: "/images/story/story-03.jpg" },
    { id: 4, src: "/images/story/story-04.jpg" },
    { id: 5, src: "/images/story/story-05.jpg" },
    { id: 6, src: "/images/story/story-06.jpg" },
    { id: 7, src: "/images/story/story-07.jpg" },
  ]);

  return (
    <>
      <div className="animate__animated animate__fadeIn">
        <div className="rounded-full flex items-center justify-center text-gray-700 hover:text-cyan-700 h-10 w-10 transition-all">
          <i
            className={`icon-love-${
              isPlaying
                ? "pause animate__animated animate__rotateIn animate__infinite animate__slow"
                : "play2"
            } text-4xl cursor-pointer`}
            onClick={() => {
              isPlaying ? howlSounds[1].pause() : howlSounds[1].play();

              setPlaying(!isPlaying);
            }}
          ></i>
        </div>
      </div>
      <Slider
        {...settingSlider}
        className="animate__animated animate__fadeIn xl:w-2/4 lg:w-3/4 w-full my-4"
      >
        {stories.map((story) => {
          return (
            <div key={story.id} className="justify-center flex-important">
              <div className="relative overflow-hidden h-32 w-28 rounded-lg shadow-sm">
                <Image
                  src={story.src}
                  objectFit="cover"
                  objectPosition="center"
                  layout="fill"
                  loading="eager"
                  alt="Story"
                />
              </div>
            </div>
          );
        })}
      </Slider>
      <h1 className="text-4xl text-center animate__animated animate__fadeIn ">
        Chúng ta đã ở bên nhau
      </h1>
      <span className=" text-3xl my-2 animate__animated animate__fadeIn">
        {moment().diff(moment("01-05-2021", "DD-MM-YYYY"), "days")}
      </span>
      <span className=" text-2xl animate__animated animate__fadeIn">Ngày</span>
      <div className="animate__animated animate__fadeIn grid grid-cols-3 gap-4 mt-10">
        <LoverCard
          loverName="Nguyễn Lương Trường Vĩ"
          color="#08979c"
          imageUrl="/images/boy.jpg"
        />

        <div className="flex items-center justify-center">
          <div className="animate__animated animate__heartBeat animate__infinite	">
            <LoveIcon width={80} height={80} />
          </div>
        </div>
        <LoverCard
          loverName="Trương Hoàng Phi Yến"
          color="#c41d7f"
          imageUrl="/images/girl.jpg"
        />
      </div>
    </>
  );
};

Introduce.propTypes = {};

export default Introduce;
