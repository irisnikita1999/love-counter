// Libraries
import "../styles/globals.scss";
import "tailwindcss/tailwind.css";
import "animate.css";

// Carousel
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
