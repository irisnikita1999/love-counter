module.exports = {
  purge: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./modules/**/*.{js,ts,jsx,tsx}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    container: {
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        lg: "4rem",
        xl: "7rem",
        "2xl": "12rem",
      },
    },
    extend: {
      boxShadow: {
        custom: "0 2px 20px -5px rgba(0, 0, 0, 0.2)",
      },
      colors: {
        "cyan-50": "#e6fffb",
        "cyan-100": "#b5f5ec",
        "cyan-200": "#87e8de",
        "cyan-300": "#5cdbd3",
        "cyan-400": "#36cfc9",
        "cyan-500": "#13c2c2",
        "cyan-600": "#08979c",
        "cyan-700": "#006d75",
        "cyan-800": "#00474f",
        "cyan-900": "#002329",
      },
      height: {
        100: "28rem",
        104: "30rem",
        106: "32.5rem",
      },
      width: {
        100: "28rem",
        104: "30rem",
        106: "32rem",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
